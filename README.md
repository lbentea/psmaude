# The PSMaude tool (version 1.0)

**Copyright (C) 2015 University of Oslo, 0316 Oslo, Norway**

**Developed by Lucian Bentea (Department of Informatics, University of Oslo)**

PSMaude, which stands for Probabilistic Strategy Language for Maude, allows the formal specification of complex *probabilistic strategies* for quantifying the nondeterminism in a general subclass of [*probabilistic rewrite theories*](http://link.springer.com/chapter/10.1007%2F978-3-540-39958-2_3) -- where the probabilistic variables are instantiated according to *discrete* finite-support probability distributions --, as well as their formal analysis. Such theories can be used to specify complex distributed systems with infinite state spaces, exhibiting both probabilistic and nondeterministic behaviors, including probabilistic real-time object-oriented systems with dynamic object and message creation, and whose functionality is defined by sophisticated algorithms. The PSMaude tool is described in more details in [this paper](http://link.springer.com/chapter/10.1007%2F978-3-642-37635-1_5).

The current version of the tool extends [Full Maude 2.6](http://maude.cs.illinois.edu/versions/2.6/FM2.6/full-maude26.maude) (see also the [Full Maude homepage](http://maude.lcc.uma.es/FullMaude/)) with a *probabilistic simulator* and a *statistical model checker* for probabilistic rewrite theories controlled by given probabilistic strategy expressions. The implementation of the PSMaude tool includes a partial formalization of the PCTL statistical model checking algorithm described in [this paper](http://link.springer.com/chapter/10.1007%2F11513988_26) and currently used by the [VeStA](http://osl.cs.uiuc.edu/~ksen/vesta2/) and [PVeStA](http://maude.cs.uiuc.edu/tools/pvesta/) tools, where properties can only be specified in a sublogic of [PCTL](http://en.wikipedia.org/wiki/Probabilistic_CTL) that excludes time-bounded operators.

The PSMaude tool also provides an *interpreter* for a wide subclass of probabilistic rewrite theories, and for the expressive probabilistic strategy language described in [this paper](http://link.springer.com/chapter/10.1007%2F978-3-642-37635-1_5), complementing PVeStA which currently executes faster, but only allows specifying a restricted class of (actor-based) probabilistic rewrite theories. Moreover, the probabilistic strategy language allows quantifying the nondeterminism in system specifications in a *modular* way -- where one system specification can be refined by several different probabilistic strategy modules --, whereas in PVeStA the schedulers that quantify the nondeterminism are combined with the system specification.

For examples on using the tool, see the [PSMaude website](http://heim.ifi.uio.no/lucianb/psmaude/)

The PSMaude tool is free software. See the file COPYING for copying permission.

## Installation

PSMaude requires both Core Maude 2.6 and Full Maude 2.6 to be installed.
These can be downloaded from: http://maude.cs.illinois.edu/versions/2.6/
and installed following the instructions here: http://maude.cs.illinois.edu/w/index.php/Maude_download_and_installation

*NOTE:* Make sure to move **full-maude26.maude** to the Core Maude directory
(where **prelude.maude** is found) so that you can load in Maude by running:

```
in full-maude26
```

PSMaude can then be started by running:
```
in psmaude
```
after starting Maude in the directory containing all PSMaude files.
